# MGC3130 Driver

A portable C driver for the Microchip [MGC3130](https://www.microchip.com/wwwproducts/en/mgc3130) e-field 3D tracking and gesture controller IC.

## Implementation

+ All library functions require a valid `mgc3130_t` type instance.
+ The `mgc3130_t` type structure has a number of function pointers that require references to user defined implementations.
+ The only optional function pointers are the gesture callbacks. If a callback is not supplied that gesture will be disabled.
+ The device i2c address must be set to a valid address.

See structure comments for more implementation details.

## Running

Continuously call `mgc3130_poll` in a loop or task.

The user defined callbacks are fired when valid gestures are detected.

## Building

This library is built with CMake. The output is a static library.

If building as part of a larger project this module can be included by using the CMake `add_subdirectory` command.