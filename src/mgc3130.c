#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "mgc3130.h"

typedef struct
{
    uint8_t msg_size;
    uint8_t flags;
    uint8_t sequence;
    uint8_t id;
} message_header_t;

typedef struct
{
    uint8_t message_id;
    const uint8_t reserved[3];
    uint8_t parameters[4];
} request_message_payload_t;

typedef struct
{
    message_header_t header;
    request_message_payload_t payload;
} request_message_t;

typedef struct
{
    uint8_t fw_valid;
    uint8_t hw_rev[2];
    uint8_t param_start_addr;
    uint8_t library_loader_version[2];
    uint8_t library_loader_platform;
    uint8_t fw_start_addr;
    uint8_t fw_version[120];
} fw_info_t;

typedef struct
{
    message_header_t header;
    fw_info_t payload;
} fw_info_message_t;

typedef struct
{
    uint8_t data_output_config_mask[2];
    uint8_t timestamp;
    uint8_t system_info;
    uint8_t dsp_status[2];
    uint8_t gesture_info[4];
    uint8_t touch_info[4];
    uint8_t airwheel_info[2];
    uint8_t xyz_position[6];
    uint8_t noise_power[4];
    uint8_t cic_data[20];
    uint8_t sd_data[20];
} sensor_data_output_t;

typedef struct
{
    message_header_t header;
    sensor_data_output_t payload;
} sensor_data_message_t;

typedef struct
{
    uint8_t runtime_parameter_id[2];
    uint8_t reserved;
    uint8_t arg_0[4];
    uint8_t arg_1[4];
} runtime_parameter_t;

typedef struct
{
    message_header_t header;
    runtime_parameter_t payload;
} runtime_parameter_message_t;

typedef struct
{
    uint8_t message_id;
    uint8_t max_message_size;
    uint8_t error_code[2];
    uint8_t reserved[8];
} system_status_t;

typedef struct
{
    message_header_t header;
    system_status_t payload;
} system_status_message_t;

static int read_msg(mgc3130_t* mgc3130, uint8_t* msg, uint32_t msg_len);
static int write_msg(mgc3130_t* mgc3130, const uint8_t* msg, uint32_t msg_len);
static void reset(mgc3130_t* mgc3130);
static int read_with_ts(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len);
static void process_message(mgc3130_t* mgc3130, sensor_data_output_t* sdo);
static int set_runtime_parameter(mgc3130_t* mgc3130, uint16_t param_id, uint32_t arg_0, uint32_t arg_1);
static int get_status_message(mgc3130_t* mgc3130);

char* mgc3130_driver_version(void)
{
    return DRIVER_VER;
}

int mgc3130_init(mgc3130_t* mgc3130)
{
    reset(mgc3130);

    if (!mgc3130->gesture_direction_callback) {
        if (set_runtime_parameter(mgc3130, 0x85, 0x00000000, 0xFFFFFFFF) == -1) {
            return -1;
        }
    } else {
        if (set_runtime_parameter(mgc3130, 0x85, 0x0000007f, 0x0000007f) == -1) {
            return -1;
        }
    }

    if (!mgc3130->gesture_touch_callback) {
        if (set_runtime_parameter(mgc3130, 0x97, 0x00, 0x01) == -1) {
            return -1;
        }
        if (set_runtime_parameter(mgc3130, 0x97, 0x00, 0x08) == -1) {
            return -1;
        }
    } else {
        if (set_runtime_parameter(mgc3130, 0x97, 0x01, 0x01) == -1) {
            return -1;
        }
        if (set_runtime_parameter(mgc3130, 0x97, 0x08, 0x08) == -1) {
            return -1;
        }
    }

    if (!mgc3130->gesture_airwheel_callback) {
        if (set_runtime_parameter(mgc3130, 0x90, 0x00, 0x20) == -1) {
            return -1;
        }
    }

    return 0;
}

void mgc3130_deinit(mgc3130_t* mgc3130)
{
    // TODO not implemented
    (void)*mgc3130;
}

int mgc3130_fw_version(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len)
{
    static const request_message_t req_message = {
        .header = {
            .msg_size = 0x0C,
            .flags = 0x00,
            .sequence = 0x00,
            .id = 0x06 },
        .payload = { .message_id = 0x83, .reserved = { 0, 0, 0 }, .parameters = { 0, 0, 0, 0 } }
    };

    int write_status = write_msg(mgc3130, (const uint8_t*)&req_message, sizeof(req_message));

    if (-1 != write_status) {
        int msg_code = read_with_ts(mgc3130, (uint8_t*)payload_buff, payload_buff_len);

        if (-1 != msg_code) {
            fw_info_message_t* fw_info_message = (fw_info_message_t*)payload_buff;

            if (0x83 == fw_info_message->header.id) {
                printf("Firmware Valid:    %s\n", fw_info_message->payload.fw_valid ? "Yes" : "No");
                printf("Hardware Revision: %d:%d\n", fw_info_message->payload.hw_rev[1], fw_info_message->payload.hw_rev[0]);
                printf("Firmware String:   %s\n", fw_info_message->payload.fw_version);
                return 0;
            }
        }
    }

    return -1;
}

int mgc3130_poll(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len)
{
    int msg_code = read_with_ts(mgc3130, payload_buff, payload_buff_len);
    sensor_data_message_t* sd = (sensor_data_message_t*)payload_buff;

    if (-1 != msg_code) {
        if (0x91 == sd->header.id) {
            process_message(mgc3130, &sd->payload);
            return 0;
        }
    }

    return -1;
}

/**
 * @brief Set runtime parameters on the device.
 * 
 * @param mgc3130 Instance
 * @param param_id Parameter ID to set
 * @param arg_0 Argument 0
 * @param arg_1 Argument 1
 * @return int Negative if write failure or incorrect status message.
 */
static int set_runtime_parameter(mgc3130_t* mgc3130, uint16_t param_id, uint32_t arg_0, uint32_t arg_1)
{
    static const message_header_t runtime_header = {
        .msg_size = 0x10,
        .flags = 0x00,
        .sequence = 0x00,
        .id = 0xA2
    };

    runtime_parameter_message_t req_message;
    memset(&req_message, 0, sizeof(req_message));

    req_message.header = runtime_header;
    memcpy(req_message.payload.runtime_parameter_id, &param_id, sizeof(uint16_t));
    memcpy(req_message.payload.arg_0, &arg_0, sizeof(uint32_t));
    memcpy(req_message.payload.arg_1, &arg_1, sizeof(uint32_t));

    int write_status = write_msg(mgc3130, (const uint8_t*)&req_message, sizeof(req_message));

    if (write_status != -1) {
        return get_status_message(mgc3130);
    }

    return -1;
}

/**
 * @brief Reads a status message from the device.
 * 
 * @param mgc3130 Instance.
 * @return int Negative if error due to non successful error code, or incorrect message id, or transfer state incorrect. 0 for success.
 */
static int get_status_message(mgc3130_t* mgc3130)
{
    system_status_message_t status_msg;
    int msg_code = read_with_ts(mgc3130, (uint8_t*)&status_msg, sizeof(status_msg));

    if (-1 != msg_code) {
        if (status_msg.header.id == 0x15) {
            if (status_msg.payload.error_code[0] == 0) {
                return 0;
            }
        }
    }

    return -1;
}

/**
 * @brief Reads a message whilst checking and setting the transfer status line line.
 * 
 * @param mgc3130 Instance
 * @param payload_buff Buffer to contain message.
 * @param payload_buff_len Length of given buffer in bytes.
 * @return int Negative if error due to transfer status being incorrect state. 0 if success.
 */
static int read_with_ts(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len)
{
    mgc3130->transfer_status_setup(true);
    if (!mgc3130->transfer_status_get_state()) {
        mgc3130->transfer_status_setup(false);
        mgc3130->transfer_status_set_state(false);

        int state = read_msg(mgc3130, payload_buff, payload_buff_len);

        mgc3130->transfer_status_set_state(true);
        mgc3130->delay_us(200);

        if (0 == state) {
            return 0;
        }
    }

    return -1;
}

/**
 * @brief Resets the device.
 * 
 * @param mgc3130 Instance.
 */
static void reset(mgc3130_t* mgc3130)
{
    mgc3130->reset(true);
    mgc3130->delay_us(100);
    mgc3130->reset(false);
    mgc3130->delay_us(100);
}

/**
 * @brief Reads a number of bytes from the device.
 * 
 * @param mgc3130 Instance
 * @param msg Buffer to contain message.
 * @param msg_len Length of given buffer in bytes.
 * @return int Negative if an error has occurred. 0 if success.
 */
static int read_msg(mgc3130_t* mgc3130, uint8_t* msg, uint32_t msg_len)
{
    return mgc3130->read(mgc3130->dev_addr, msg, msg_len);
}

/**
 * @brief Writes a number of bytes to the device.
 * 
 * @param mgc3130 Instance
 * @param msg Buffer to contain message.
 * @param msg_len Length of given buffer in bytes.
 * @return int Negative if an error has occurred. 0 if success.
 */
static int write_msg(mgc3130_t* mgc3130, const uint8_t* msg, uint32_t msg_len)
{
    return mgc3130->write(mgc3130->dev_addr, msg, msg_len);
}

/**
 * @brief Processes the sensor data output. If a gesture has been detected the appropriate callback is called.
 * 
 * @param mgc3130 Instance.
 * @param sdo Pointer to buffer containing sensor data output.
 */
static void process_message(mgc3130_t* mgc3130, sensor_data_output_t* sdo)
{
    static const uint8_t POSITION_VALID = (1 << 0);
    static const uint8_t AIRWHEEL_VALID = (1 << 1);

    static const uint8_t DATA_OUTPUT_GESTURE = (1 << 1);
    static const uint8_t DATA_OUTPUT_TOUCH = (1 << 2);
    static const uint8_t DATA_OUTPUT_AIRWHEEL = (1 << 3);
    static const uint8_t DATA_OUTPUT_XYZ = (1 << 4);

    uint16_t config_mask;
    memcpy(&config_mask, sdo->data_output_config_mask, sizeof(config_mask));

    if (config_mask & DATA_OUTPUT_XYZ && sdo->system_info & POSITION_VALID) {
        uint16_t xyz[3];
        memcpy(xyz, &sdo->xyz_position[0], sizeof(xyz));

        if (mgc3130->gesture_xyz_callback) {
            mgc3130->gesture_xyz_callback(xyz[0], xyz[1], xyz[2]);
        }
    }

    if (config_mask & DATA_OUTPUT_GESTURE && sdo->gesture_info[0] > 0) {
        if (mgc3130->gesture_direction_callback) {
            mgc3130->gesture_direction_callback(sdo->gesture_info[0]);
        }
    }

    if (config_mask & DATA_OUTPUT_TOUCH) {
        uint16_t touch_action = ((uint16_t)sdo->touch_info[1]) << 8 | sdo->touch_info[0];
        uint8_t touch_time = sdo->touch_info[2];
        uint16_t moving_time_ms = (touch_time * 5);

        int8_t touch_location = -1;

        if (touch_action & 0x1F) {
            touch_location = 0;
        } else if ((touch_action >> 5) & 0x1F) {
            touch_location = 1;
        } else if ((touch_action >> 10) & 0x1F) {
            touch_location = 2;
        }

        if (touch_location != -1) {
            for (uint8_t direction = 0; direction < 5; direction++) {
                if ((1 << direction) & (touch_action >> ((uint8_t)touch_location * 5))) {
                    if (mgc3130->gesture_touch_callback) {
                        mgc3130->gesture_touch_callback((uint8_t)touch_location, direction, moving_time_ms);
                    }
                }
            }
        }
    }

    if (config_mask & DATA_OUTPUT_AIRWHEEL && sdo->system_info & AIRWHEEL_VALID) {
        static const float POSITION_INCREMENT = 360.0f / 32.0f;

        uint8_t angular_position = sdo->airwheel_info[0] & 0x1F;
        uint8_t rotations = (sdo->airwheel_info[0] >> 5) & 0x07;

        float angle = POSITION_INCREMENT * (float)angular_position;

        if (mgc3130->gesture_airwheel_callback) {
            mgc3130->gesture_airwheel_callback(angle, rotations);
        }
    }
}