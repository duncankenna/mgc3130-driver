#pragma once

#include <stdbool.h>
#include <stdint.h>

#define MGC3130_TOUCH_LOCATION_SOUTH 0
#define MGC3130_TOUCH_LOCATION_WEST 1
#define MGC3130_TOUCH_LOCATION_NORTH 2
#define MGC3130_TOUCH_LOCATION_EAST 3
#define MGC3130_TOUCH_LOCATION_CENTRE 4

#define MGC3130_TOUCH_TYPE_TOUCH 0
#define MGC3130_TOUCH_TYPE_TAP 1
#define MGC3130_TOUCH_TYPE_DOUBLETAP 2

#define MGC3130_GESTURE_FLICK_WEST_TO_EAST 2
#define MGC3130_GESTURE_FLICK_EAST_TO_WEST 3
#define MGC3130_GESTURE_FLICK_SOUTH_TO_NORTH 4
#define MGC3130_GESTURE_FLICK_NORTH_TO_SOUTH 5

typedef struct
{
    // Device i2c address
    uint8_t dev_addr;

    // i2c Read and Write. Return -1 for error. 0 for success
    int (*read)(uint8_t dev_addr, uint8_t* data, uint32_t data_len);
    int (*write)(uint8_t dev_addr, const uint8_t* data, uint32_t data_len);

    // Reset line. True state represents reset active
    void (*reset)(bool state);

    // Transfer status pin direction. True represents pin set as input.
    void (*transfer_status_setup)(bool input);
    // Transfer status pin set output state. True represents state as high.
    void (*transfer_status_set_state)(bool state);
    // Transfer status pin get output state. True represents state as high.
    bool (*transfer_status_get_state)(void);

    // Microsecond delay. Parameter state how many microseconds to delay for.
    void (*delay_us)(uint32_t us);

    // Optional gesture detected callbacks
    void (*gesture_xyz_callback)(uint16_t x, uint16_t y, uint16_t z);
    void (*gesture_direction_callback)(uint8_t gesture_id);
    void (*gesture_touch_callback)(uint8_t touch_type, uint8_t location, uint16_t moving_time_ms);
    void (*gesture_airwheel_callback)(float angular_position, uint8_t rotations);

} mgc3130_t;

/**
 * @brief Get driver version.
 * 
 * @return char* Driver version string.
 */
char* mgc3130_driver_version(void);

/**
 * @brief Initialise the mgc3130. Any callback functions not set (NULL) will disable detection of that feature.
 * 
 * @param mgc3130 Instance
 * @return int Negative if an error has occurred. 0 for success.
 */
int mgc3130_init(mgc3130_t* mgc3130);

/**
 * @brief De-initialises the mgc3130.
 * 
 * @param mgc3130 Instance.
 */
void mgc3130_deinit(mgc3130_t* mgc3130);

/**
 * @brief Reads the device firmware version information.
 * 
 * @param mgc3130 Instance.
 * @param payload_buff Buffer for storing retrieved command data.
 * @param payload_buff_len Length of buffer in bytes.
 * @return int Negative due to device not being ready. 0 for success.
 */
int mgc3130_fw_version(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len);

/**
 * @brief Polls the device for gesture information. If a gesture is enabled the corresponding callback will be called. 
 * 
 * @param mgc3130 Instance
 * @param payload_buff Buffer for storing retrieved command data.
 * @param payload_buff_len Length of buffer in bytes.
 * @return int Negative due to device not being ready. 0 for success.
 */
int mgc3130_poll(mgc3130_t* mgc3130, uint8_t* payload_buff, uint32_t payload_buff_len);
